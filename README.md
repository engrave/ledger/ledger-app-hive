# @engrave/ledger-app-hive

Ledger Hardware Wallet HIVE JavaScript/TypeScript bindings.

# Example usage

```typescript
import Hive from '@engrave/ledger-app-hive';
import TransportNodeHid from '@ledgerhq/hw-transport-node-hid';

const transport = await TransportNodeHid.create();
const hive = new Hive(transport);

const version = await hive.getAppVersion();
console.log("Current version:", version);

const name = await hive.getAppName();
console.log("App name:", name);

const publicKey = await hive.getPublicKey(`48'/13'/0'/0'/0'`);
console.log("Public key:", publicKey);

const signature = await hive.signMessage('This is a message to sign', `48'/13'/0'/0'/0'`);
console.log("Signature:", signature);
```

For more examples, visit `examples` directory.

## API

 * [getAppVersion](#getappversion)
 * [getAppName](#getappname)
 * [getPublicKey](#getpublickey)
 * [getSettings](#getsettings)
 * [signTransaction](#signtransaction) 
 * [signHash](#signhash) 
 * [signMessage](#signmessage) 

## getAppVersion

Get current version for HIVE App installed on a Ledger.

```typescript
async getAppVersion(): Promise<string>
```

## getAppName

Get name for HIVE App installed on a Ledger.

```typescript
async getAppName(): Promise<string>
```

## getPublicKey

Retrieve public key from specified BIP32 path. You can set additional parameter `confirm: boolean` which will ask the user to confirm the Public Key on his Ledger before returning it. 

```typescript
async getPublicKey(path: string, confirm?: boolean): Promise<string>
```

## getSettings

Read settings from Ledger. This can be used to determine if the user has enabled the "blind signing" option in the HIVE App.

```typescript
async getSettings(): Promise<Settings>
```

## signTransaction

Sign basic transaction with specified key path. This method will return signed transaction (transaction with additional `signatures` property). Please note that extensions are not yet supported for most transactions and operations (except `beneficiaries` for `comment_options` and `end_date` for `update_proposal`). Transaction cannot contain more than one operation, otherwise ledger will refuse the transaction. 

```typescript
async signTransaction(tx: Transaction, path: string, chainId?: string): Promise<SignedTransaction>
```

You can also provide optional `chainId` which may be useful for tests on Hive testnet. If not provided, Ledger will use production chainId.

## signHash

Sign transaction digest with specified key path. This method will return a single signature. Please note that this method requires `blind signing` mode enabled in settings, otherwise ledger will refuse it. 

```typescript
async signHash(digest: string, path: string): Promise<string>
```

Transaction need to he serialized with proper `chainId` up front. This can be done with static method called `getTransactionDigest` 

## signMessage

Sign message with specified key path. This method will return a single signature. May be used to prove ownership of a private key. 

```typescript
async signMessage(message: string, path: string): Promise<string>
```

Please note there is a limit of 512 characters for the message.
