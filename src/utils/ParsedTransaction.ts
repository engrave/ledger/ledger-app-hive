import {ParsedOperation} from './ParsedOperation';
import {Encoder} from './encoders/common/Encoder';
import {Transaction} from '@hiveio/dhive';
import {sha256} from '@noble/hashes/sha256';

const asn = require('asn1.js');

export class ParsedTransaction {

    private rawTx: Transaction;
    private chainId: Buffer;
    private ref_block_num!: Buffer;
    private ref_block_prefix!: Buffer;
    private expiration!: Buffer;
    private operations_count!: Buffer;
    private operations!: Buffer[];
    private extensions_size!: Buffer;
    private encoder: any;

    constructor(tx: Transaction, chainId: string = 'beeab0de00000000000000000000000000000000000000000000000000000000') {
        this.rawTx = tx;

        this.encoder = asn.define('LedgerEncoder', function () {
            // @ts-ignore
            return this.octstr()
        });

        this.chainId = Buffer.from(chainId, 'hex')
        this.ref_block_num = Encoder.encodeUInt16(this.rawTx.ref_block_num & 0xffff);
        this.ref_block_prefix = Encoder.encodeUInt32(this.rawTx.ref_block_prefix);
        this.expiration = Encoder.encodeDateTime(this.rawTx.expiration);
        this.extensions_size = Encoder.encodeUInt8(this.rawTx.extensions.length);
        this.operations_count = Encoder.encodeUInt8(this.rawTx.operations.length);
        this.operations = this.rawTx.operations.map(ParsedOperation.encode);
    }

    public encode = (): Buffer => {
        const encodedOperations = this.operations.reduce((result: Buffer, op) => Buffer.concat([result, this.encoder.encode(op, 'der')]), Buffer.alloc(0))
        const data = Buffer.concat([
            this.encoder.encode(this.chainId, 'der'),
            this.encoder.encode(this.ref_block_num, 'der'),
            this.encoder.encode(this.ref_block_prefix, 'der'),
            this.encoder.encode(this.expiration, 'der'),
            this.encoder.encode(this.operations_count, 'der'),
            encodedOperations,
            this.encoder.encode(this.extensions_size, 'der')
        ])

        return data;
    }

    public digest (): string {
        const data = Buffer.concat([
            this.chainId,
            this.ref_block_num,
            this.ref_block_prefix,
            this.expiration,
            this.operations_count,
            this.operations.reduce((result: Buffer, op) => Buffer.concat([result, op]), Buffer.alloc(0)),
            this.extensions_size
        ]);
        return Buffer.from(sha256(data)).toString('hex');
    }
}
