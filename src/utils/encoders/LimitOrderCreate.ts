import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {LimitOrderCreateOperation} from "@hiveio/dhive";

export class LimitOrderCreate extends OperationEncoder {
    encode(payload: LimitOrderCreateOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.owner),
            Encoder.encodeUInt32(payload.orderid),
            Encoder.encodeAsset(payload.amount_to_sell),
            Encoder.encodeAsset(payload.min_to_receive),
            Encoder.encodeBoolean(payload.fill_or_kill),
            Encoder.encodeDateTime(payload.expiration)
        ])
    }

    validate(payload: LimitOrderCreateOperation[1]): boolean {
        return Validator.validateString(payload.owner)
    }
}
