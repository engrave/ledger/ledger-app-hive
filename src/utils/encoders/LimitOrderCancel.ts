import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {LimitOrderCancelOperation} from "@hiveio/dhive";

export class LimitOrderCancel extends OperationEncoder {
    encode(payload: LimitOrderCancelOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.owner),
            Encoder.encodeUInt32(payload.orderid)
        ])
    }

    validate(payload: LimitOrderCancelOperation[1]): boolean {
        return Validator.validateString(payload.owner)
    }
}
