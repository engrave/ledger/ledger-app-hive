import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {AccountCreateOperation} from "@hiveio/dhive";

export class AccountCreate extends OperationEncoder {
    encode(payload: AccountCreateOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeAsset(payload.fee),
            Encoder.encodeString(payload.creator),
            Encoder.encodeString(payload.new_account_name),
            Encoder.encodeAuthorityType(payload.owner),
            Encoder.encodeAuthorityType(payload.active),
            Encoder.encodeAuthorityType(payload.posting),
            Encoder.encodePublicKey(payload.memo_key),
            Encoder.encodeString(payload.json_metadata),
        ])
    }

    validate(payload: AccountCreateOperation[1]): boolean {
        return Validator.validateString(payload.creator) &&
            Validator.validateString(payload.new_account_name) &&
            Validator.validateAuthorityType(payload.owner) &&
            Validator.validateAuthorityType(payload.active) &&
            Validator.validateAuthorityType(payload.posting) &&
            Validator.validateString(payload.json_metadata)
    }
}

