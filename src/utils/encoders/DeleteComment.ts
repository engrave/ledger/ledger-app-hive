import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {DeleteCommentOperation} from "@hiveio/dhive";

export class DeleteComment extends OperationEncoder {
    encode(payload: DeleteCommentOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.author),
            Encoder.encodeString(payload.permlink)
        ])
    }

    validate(payload: DeleteCommentOperation[1]): boolean {
        return Validator.validateString(payload.author) && Validator.validateString(payload.permlink)
    }
}
