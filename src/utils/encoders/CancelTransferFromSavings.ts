import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {CancelTransferFromSavingsOperation} from "@hiveio/dhive";

export class CancelTransferFromSavings extends OperationEncoder {
    encode(payload: CancelTransferFromSavingsOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.from),
            Encoder.encodeUInt32(payload.request_id)
        ])
    }

    validate(payload: CancelTransferFromSavingsOperation[1]): boolean {
        return Validator.validateString(payload.from)
    }
}
