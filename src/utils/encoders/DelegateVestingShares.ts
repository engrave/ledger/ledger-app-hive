import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {DelegateVestingSharesOperation} from "@hiveio/dhive";

export class DelegateVestingShares extends OperationEncoder {
    encode(payload: DelegateVestingSharesOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.delegator),
            Encoder.encodeString(payload.delegatee),
            Encoder.encodeAsset(payload.vesting_shares)
        ])
    }

    validate(payload: DelegateVestingSharesOperation[1]): boolean {
        return Validator.validateString(payload.delegator) && Validator.validateString(payload.delegatee)
    }
}
