import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {WithdrawVestingOperation} from "@hiveio/dhive";
import {Validator} from "./common/Validator";

export class WithdrawVesting extends OperationEncoder {
    encode(payload: WithdrawVestingOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.account),
            Encoder.encodeAsset(payload.vesting_shares)
        ])
    }

    validate(payload: WithdrawVestingOperation[1]): boolean {
        return Validator.validateString(payload.account)
    }
}
