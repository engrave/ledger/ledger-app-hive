import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {ClaimRewardBalanceOperation} from "@hiveio/dhive";

export class ClaimRewardBalance extends OperationEncoder {
    encode(payload: ClaimRewardBalanceOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.account),
            Encoder.encodeAsset(payload.reward_hive),
            Encoder.encodeAsset(payload.reward_hbd),
            Encoder.encodeAsset(payload.reward_vests)
        ])
    }

    validate(payload: ClaimRewardBalanceOperation[1]): boolean {
        return Validator.validateString(payload.account)
    }
}

