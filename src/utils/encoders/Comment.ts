import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {CommentOperation} from "@hiveio/dhive";

export class Comment extends OperationEncoder {
    encode(payload: CommentOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.parent_author),
            Encoder.encodeString(payload.parent_permlink),
            Encoder.encodeString(payload.author),
            Encoder.encodeString(payload.permlink),
            Encoder.encodeString(payload.title),
            Encoder.encodeString(payload.body),
            Encoder.encodeString(payload.json_metadata)
        ])
    }

    validate(payload: CommentOperation[1]): boolean {
        return Validator.validateString(payload.parent_author) &&
            Validator.validateString(payload.parent_permlink) &&
            Validator.validateString(payload.author) &&
            Validator.validateString(payload.permlink) &&
            Validator.validateString(payload.title) &&
            Validator.validateString(payload.body) &&
            Validator.validateString(payload.json_metadata)
    }
}
