import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {WitnessUpdateOperation} from "@hiveio/dhive";
import {Validator} from "./common/Validator";

export class WitnessUpdate extends OperationEncoder {
    encode(payload: WitnessUpdateOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.owner),
            Encoder.encodeString(payload.url),
            Encoder.encodePublicKey(payload.block_signing_key!), // TODO verify if this field is optional
            Encoder.encodeAsset(payload.props.account_creation_fee),
            Encoder.encodeUInt32(payload.props.maximum_block_size),
            Encoder.encodeUInt16(payload.props.hbd_interest_rate),
            Encoder.encodeAsset(payload.fee)
        ])
    }

    validate(payload: WitnessUpdateOperation[1]): boolean {
        return Validator.validateString(payload.owner) &&
            Validator.validateString(payload.url)
    }
}

