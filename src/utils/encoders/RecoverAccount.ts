import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {RecoverAccountOperation} from '@hiveio/dhive';
import {Validator} from "./common/Validator";

export class RecoverAccount extends OperationEncoder {
    encode(payload: RecoverAccountOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.account_to_recover),
            Encoder.encodeAuthorityType(payload.new_owner_authority),
            Encoder.encodeAuthorityType(payload.recent_owner_authority),
            Encoder.Array(Encoder.encodeEmpty, payload.extensions)
        ])
    }

    validate(payload: RecoverAccountOperation[1]): boolean {
        return Validator.validateString(payload.account_to_recover) &&
            Validator.validateAuthorityType(payload.new_owner_authority) &&
            Validator.validateAuthorityType(payload.recent_owner_authority)
    }

}

