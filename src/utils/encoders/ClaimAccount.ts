import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {ClaimAccountOperation} from "@hiveio/dhive";

export class ClaimAccount extends OperationEncoder {
    encode(payload: ClaimAccountOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.creator),
            Encoder.encodeAsset(payload.fee),
            Encoder.Array(Encoder.encodeEmpty, payload.extensions)
        ])
    }

    validate(payload: ClaimAccountOperation[1]): boolean {
        return Validator.validateString(payload.creator)
    }
}
