import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {CollateralizedConvertOperation} from '@hiveio/dhive';
import {Validator} from "./common/Validator";

export class CollateralizedConvert extends OperationEncoder {
    encode(payload: CollateralizedConvertOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.owner),
            Encoder.encodeUInt32(payload.requestid),
            Encoder.encodeAsset(payload.amount),
        ])
    }

    validate(payload: CollateralizedConvertOperation[1]): boolean {
        return Validator.validateString(payload.owner)
    }
}

