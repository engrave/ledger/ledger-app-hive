import {AuthorityType} from "@hiveio/dhive/lib/chain/account";
import {PublicKey} from "@hiveio/dhive/lib/crypto";
import {BeneficiaryRoute} from "@hiveio/dhive/lib/chain/comment";

const MAX_STRING_LENGTH = 127;

export class Validator {

    public static validateString = (str: string): boolean => {
        return str.length <= MAX_STRING_LENGTH;
    }

    public static validateAuthorityType = (a?: AuthorityType ): boolean => {
        if (!a) return true;
        const account_auths = a.account_auths.map((auth: [string, number]) => `[ ${auth[0]}, ${auth[1]} ]`).join(', ');
        const key_auths = a.key_auths.map((auth: [string | PublicKey, number]) => `[ ${auth[0]}, ${auth[1]} ]`).join(', ');
        const message = `Weight: ${a.weight_threshold}, [ ${account_auths} ], [ ${key_auths} ]`;
        return Validator.validateString(message);
    }

    public static validateArrayOfStrings = (arr: string[]): boolean => {
        const str = `[ ${arr.join(', ')} ]`;
        return Validator.validateString(str);
    }

    public static validateArrayOfUInt64 = (arr: number[]): boolean => {
        const str = `[ ${arr.join(', ')} ]`;
        return Validator.validateString(str);
    }

    public static validateBeneficiaries = (route: BeneficiaryRoute[]): boolean => {
        const str = `Beneficiaries: [ ${route.map((b) => `[ ${b.account}, ${(b.weight/100).toFixed(2)}% ]`).join(', ')} ]`;
        return Validator.validateString(str);
    }
}
