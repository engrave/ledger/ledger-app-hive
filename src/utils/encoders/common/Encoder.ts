import moment from "moment";
import {Asset, AuthorityType} from '@hiveio/dhive';
import {PublicKey} from '@hiveio/dhive/lib/crypto';
import * as varint from 'varint';

const bs58 = require('bs58');

export class Encoder {

    private static readonly precisionTable: Record<string, number> = {
        "HIVE": 3,
        "HBD": 3,
        "STEEM": 3,
        "SBD": 3,
        "TBD": 3,
        "TESTS": 3,
        "VESTS": 6
    }

    public static encodeOperationType = (type: number): Buffer => {
        return Buffer.alloc(1, type);
    }

    public static encodeBoolean = (value: boolean): Buffer => {
        return Buffer.from([value ? 1 : 0]);
    }

    public static encodeDateTime = (date: string): Buffer => {
        const timestamp = moment.utc(date).valueOf() / 1000;
        return Encoder.encodeUInt32(timestamp);
    }

    public static encodeString = (str: string): Buffer => {
        if (!str) {
            return Buffer.from([0]);
        }
        const buf = Buffer.concat([
            Encoder.encodeVarint32(str.length),
            Buffer.from(str)]);

        return buf;
    }

    public static encodeUInt8 = (value: number): Buffer => {
        const buf = Buffer.alloc(1);
        buf.writeUInt8(value);
        return buf;
    }

    public static encodeUInt16 = (value: number): Buffer => {
        const buf = Buffer.alloc(2);
        buf.writeUInt16LE(value);
        return buf;
    }

    public static encodeInt16 = (value: number): Buffer => {
        const buf = Buffer.alloc(2);
        buf.writeInt16LE(value);
        return buf;
    }

    public static encodeUInt32 = (value: number): Buffer => {
        const buf = Buffer.alloc(4);
        buf.writeUInt32LE(value);
        return buf;
    }

    public static encodeUInt64 = (value: number): Buffer => {
        const buf = Buffer.alloc(8);
        let hex = value.toString(16); // change to hex representation
        while (hex.length < 16) // pad with leading zeros
            hex = '0' + hex;
        buf.write(hex, 0, 8, 'hex');
        return buf.reverse(); // reverse the byte order
    }

    public static encodeVarint32 = (value: number): Buffer => {
        return Buffer.from(varint.encode(value));
    }

    static encodeAsset = (asset: string | Asset): Buffer => {
        const data = Buffer.alloc(8 + 1 + 7);
        const [amount, symbol] = (asset as string).split(' ');
        const precision = Encoder.precisionTable[Encoder.convertAssetSybol(symbol)];
        let convertedAmount = Math.round(parseFloat(amount) * Math.pow(10, precision))

        Encoder.encodeUInt64(convertedAmount).copy(data, 0);
        data.writeUInt8(precision, 8);

        for (let i = 0; i < Encoder.convertAssetSybol(symbol).length; i++) {
            data.writeUInt8(Encoder.convertAssetSybol(symbol).charCodeAt(i), 9 + i);
        }

        return data;
    }

    public static Array = <T>(encoder: (value: T) => Buffer, values: T[] | undefined): Buffer => {
        return Buffer.concat([
            Encoder.encodeVarint32(values?.length ?? 0),
            ...values?.map(encoder) ?? []
        ]);
    }

    public static Optional = <T>(encoder: (value: T) => Buffer, value: T | undefined): Buffer => {
        if (value) {
            return Buffer.concat([Buffer.from([1]), encoder(value)]);
        } else {
            return Buffer.from([0]);
        }
    }

    public static encodePublicKey = (key: string | PublicKey): Buffer => {
        const data = (key as string).slice(3);
        const decoded = bs58.decode(data);
        return decoded.slice(0, decoded.length - 4);
    }

    public static encodeAuthorityType = (auths: AuthorityType): Buffer => {
        return Buffer.concat([
            Encoder.encodeUInt32(auths.weight_threshold),
            Encoder.Array((account: any) => Buffer.concat([
                Encoder.encodeString(account[0]),
                Encoder.encodeUInt16(account[1])
            ]), auths.account_auths),
            Encoder.Array((key: any) =>
                Buffer.concat([
                    Encoder.encodePublicKey(key[0]),
                    Encoder.encodeUInt16(key[1])
                ]), auths.key_auths),
        ]);
    }

    public static encodeEmpty = (): Buffer => {
        throw new Error('This field is not supported yet.');
    }

    private static convertAssetSybol = (symbol: string) => {
        switch (symbol) {
            case 'HIVE':
                return "STEEM";
                break;
            case "HBD":
                return "SBD";
                break;
            default:
                return symbol;
        }
    }

}
