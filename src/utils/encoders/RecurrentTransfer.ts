import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {RecurrentTransferOperation} from '@hiveio/dhive';
import {Validator} from "./common/Validator";

export class RecurrentTransfer extends OperationEncoder {
    encode(payload: RecurrentTransferOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.from),
            Encoder.encodeString(payload.to),
            Encoder.encodeAsset(payload.amount),
            Encoder.encodeString(payload.memo),
            Encoder.encodeUInt16(payload.recurrence),
            Encoder.encodeUInt16(payload.executions),
            Encoder.Array(Encoder.encodeEmpty, payload.extensions)
        ])
    }

    validate(payload: any): boolean {
        return Validator.validateString(payload.from) &&
            Validator.validateString(payload.to) &&
            Validator.validateString(payload.memo)
    }
}

