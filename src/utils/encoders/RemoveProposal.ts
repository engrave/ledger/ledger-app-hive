import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {RemoveProposalOperation} from "@hiveio/dhive";

export class RemoveProposal extends OperationEncoder {
    encode(payload: RemoveProposalOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.proposal_owner),
            Encoder.Array(Encoder.encodeUInt64, payload.proposal_ids),
            Encoder.Array(Encoder.encodeEmpty, payload.extensions)
        ])
    }

    validate(payload: RemoveProposalOperation[1]): boolean {
        return Validator.validateString(payload.proposal_owner) && Validator.validateArrayOfUInt64(payload.proposal_ids)
    }
}

