import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {CommentOptionsOperation} from '@hiveio/dhive';
import {Validator} from "./common/Validator";
import {BeneficiaryRoute} from "@hiveio/dhive/lib/chain/comment";

export class CommentOptions extends OperationEncoder {
    encode(payload: CommentOptionsOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.author),
            Encoder.encodeString(payload.permlink),
            Encoder.encodeAsset(payload.max_accepted_payout),
            Encoder.encodeUInt16(payload.percent_hbd),
            Encoder.encodeBoolean(payload.allow_votes),
            Encoder.encodeBoolean(payload.allow_curation_rewards),
            this.encodeExtensions(payload.extensions)
        ])
    }

    private encodeExtensions = (extensions: [0, { beneficiaries: BeneficiaryRoute[]}][]): Buffer => {

        if (!extensions || extensions.length === 0) {
            return Buffer.from([extensions ? extensions.length : [].length])
        }

        // WARNING only one extension is currently supported: beneficiaries
        if (extensions[0][0] !== 0) {
            throw new Error('Unsupported extension type');
        }

        return Buffer.concat([
            Encoder.encodeVarint32(extensions.length),
            Encoder.encodeVarint32(extensions[0][0]),
            Encoder.Array(this.encodeBeneficiary, extensions[0][1].beneficiaries)
        ])
    }

    private encodeBeneficiary = (beneficiary: any): Buffer => {
        return Buffer.concat([
            Encoder.encodeString(beneficiary.account),
            Encoder.encodeUInt16(beneficiary.weight)
        ])
    }

    validate(payload: CommentOptionsOperation[1]): boolean {
        let result = Validator.validateString(payload.author) && Validator.validateString(payload.permlink);

        if (payload.extensions && payload.extensions.length > 0) {
            result = result && Validator.validateBeneficiaries(payload.extensions[0][1].beneficiaries);
        }

        return result;
    }
}

