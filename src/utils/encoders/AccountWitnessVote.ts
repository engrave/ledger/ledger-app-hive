import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {AccountWitnessVoteOperation} from "@hiveio/dhive";

export class AccountWitnessVote extends OperationEncoder {
    encode(payload: AccountWitnessVoteOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.account),
            Encoder.encodeString(payload.witness),
            Encoder.encodeBoolean(payload.approve)
        ])
    }

    validate(payload: AccountWitnessVoteOperation[1]): boolean {
        return Validator.validateString(payload.account) && Validator.validateString(payload.witness);
    }
}
