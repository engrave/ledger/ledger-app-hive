import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {CreateProposalOperation} from '@hiveio/dhive';
import {Validator} from "./common/Validator";

export class CreateProposal extends OperationEncoder {
    encode(payload: CreateProposalOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.creator),
            Encoder.encodeString(payload.receiver),
            Encoder.encodeDateTime(payload.start_date),
            Encoder.encodeDateTime(payload.end_date),
            Encoder.encodeAsset(payload.daily_pay),
            Encoder.encodeString(payload.subject),
            Encoder.encodeString(payload.permlink),
            Encoder.Array(Encoder.encodeEmpty, payload.extensions)
        ])
    }

    validate(payload: CreateProposalOperation[1]): boolean {
        return Validator.validateString(payload.creator) &&
            Validator.validateString(payload.receiver) &&
            Validator.validateString(payload.subject) &&
            Validator.validateString(payload.permlink)
    }

}

