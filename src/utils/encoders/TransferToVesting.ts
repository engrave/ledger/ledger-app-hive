import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {TransferToVestingOperation} from "@hiveio/dhive";
import {Validator} from "./common/Validator";

export class TransferToVesting extends OperationEncoder {
    encode(payload: TransferToVestingOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.from),
            Encoder.encodeString(payload.to),
            Encoder.encodeAsset(payload.amount)
        ])
    }

    validate(payload: TransferToVestingOperation[1]): boolean {
        return Validator.validateString(payload.from) && Validator.validateString(payload.to)
    }
}
