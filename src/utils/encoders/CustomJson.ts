import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {CustomJsonOperation} from "@hiveio/dhive";

export class CustomJson extends OperationEncoder {
    encode(payload: CustomJsonOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.Array(Encoder.encodeString, payload.required_auths),
            Encoder.Array(Encoder.encodeString, payload.required_posting_auths),
            Encoder.encodeString(payload.id),
            Encoder.encodeString(payload.json)
        ])
    }

    validate(payload: any): boolean {
        return Validator.validateArrayOfStrings(payload.required_auths) &&
            Validator.validateArrayOfStrings(payload.required_posting_auths) &&
            Validator.validateString(payload.id) &&
            Validator.validateString(payload.json)
    }
}

