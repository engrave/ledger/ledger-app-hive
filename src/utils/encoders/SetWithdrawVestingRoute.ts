import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {SetWithdrawVestingRouteOperation} from "@hiveio/dhive";
import {Validator} from "./common/Validator";

export class SetWithdrawVestingRoute extends OperationEncoder {
    encode(payload: SetWithdrawVestingRouteOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.from_account),
            Encoder.encodeString(payload.to_account),
            Encoder.encodeUInt16(Number(payload.percent)),
            Encoder.encodeBoolean(payload.auto_vest)
        ])
    }

    validate(payload: SetWithdrawVestingRouteOperation[1]): boolean {
        return Validator.validateString(payload.from_account) &&
            Validator.validateString(payload.to_account)
    }
}
