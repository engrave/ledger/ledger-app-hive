import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {VoteOperation} from "@hiveio/dhive";
import {Validator} from "./common/Validator";

export class Vote extends OperationEncoder {
    encode(payload: VoteOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.voter),
            Encoder.encodeString(payload.author),
            Encoder.encodeString(payload.permlink),
            Encoder.encodeInt16(payload.weight)
        ])
    }

    validate(payload: VoteOperation[1]): boolean {
        return Validator.validateString(payload.voter) &&
            Validator.validateString(payload.author) &&
            Validator.validateString(payload.permlink)
    }
}
