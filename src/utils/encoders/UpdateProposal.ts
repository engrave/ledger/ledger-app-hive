import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {UpdateProposalOperation} from '@hiveio/dhive';
import {Validator} from "./common/Validator";

export class UpdateProposal extends OperationEncoder {
    encode(payload: UpdateProposalOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeUInt64(payload.proposal_id),
            Encoder.encodeString(payload.creator),
            Encoder.encodeAsset(payload.daily_pay),
            Encoder.encodeString(payload.subject),
            Encoder.encodeString(payload.permlink),
            this.encodeExtensions(payload.extensions)
        ])
    }

    private encodeExtensions = (extensions: Array<any>): Buffer => {

        if (!extensions || extensions.length === 0) {
            return Buffer.from([extensions ? extensions.length : [].length])
        }

        // WARNING only one extension is currently supported: end_date
        if (extensions[0][0] !== 1) {
            throw new Error('Unsupported extension type');
        }
        return Buffer.concat([
            Encoder.encodeUInt8(extensions.length),
            Encoder.encodeUInt8(extensions[0][0]),
            Encoder.encodeDateTime(extensions[0][1].end_date)
        ])
    }

    validate(payload: UpdateProposalOperation[1]): boolean {
        return Validator.validateString(payload.creator) &&
            Validator.validateString(payload.subject) &&
            Validator.validateString(payload.permlink)
    }
}

