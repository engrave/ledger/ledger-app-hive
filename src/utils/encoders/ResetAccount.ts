import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {ResetAccountOperation} from "@hiveio/dhive";
import {Validator} from "./common/Validator";

export class ResetAccount extends OperationEncoder {
    encode(payload: ResetAccountOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.reset_account),
            Encoder.encodeString(payload.account_to_reset),
            Encoder.encodeAuthorityType(payload.new_owner_authority)
        ])
    }

    validate(payload: ResetAccountOperation[1]): boolean {
        return Validator.validateString(payload.reset_account) &&
            Validator.validateString(payload.account_to_reset) &&
            Validator.validateAuthorityType(payload.new_owner_authority)
    }
}

