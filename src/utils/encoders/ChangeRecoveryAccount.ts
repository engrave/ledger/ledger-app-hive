import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {ChangeRecoveryAccountOperation} from "@hiveio/dhive";

export class ChangeRecoveryAccount extends OperationEncoder {
    encode(payload: ChangeRecoveryAccountOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.account_to_recover),
            Encoder.encodeString(payload.new_recovery_account),
            Encoder.Array(Encoder.encodeEmpty, payload.extensions)
        ])
    }

    validate(payload: ChangeRecoveryAccountOperation[1]): boolean {
        return Validator.validateString(payload.account_to_recover) && Validator.validateString(payload.new_recovery_account)
    }
}
