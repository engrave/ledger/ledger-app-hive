import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {ConvertOperation} from "@hiveio/dhive";

export class Convert extends OperationEncoder {
    encode(payload: ConvertOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.owner),
            Encoder.encodeUInt32(payload.requestid),
            Encoder.encodeAsset(payload.amount)
        ])
    }

    validate(payload: ConvertOperation[1]): boolean {
        return Validator.validateString(payload.owner)
    }
}
