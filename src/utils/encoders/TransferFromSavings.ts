import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {TransferFromSavingsOperation} from "@hiveio/dhive";
import {Validator} from "./common/Validator";

export class TransferFromSavings extends OperationEncoder {
    encode(payload: TransferFromSavingsOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.from),
            Encoder.encodeUInt32(payload.request_id),
            Encoder.encodeString(payload.to),
            Encoder.encodeAsset(payload.amount),
            Encoder.encodeString(payload.memo)
        ])
    }

    validate(payload: TransferFromSavingsOperation[1]): boolean {
        return Validator.validateString(payload.from) &&
            Validator.validateString(payload.to) &&
            Validator.validateString(payload.memo)
    }
}
