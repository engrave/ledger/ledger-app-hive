import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {FeedPublishOperation} from "@hiveio/dhive";

export class FeedPublish extends OperationEncoder {
    encode(payload: FeedPublishOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.publisher),
            Encoder.encodeAsset(payload.exchange_rate.base),
            Encoder.encodeAsset(payload.exchange_rate.quote)
        ])
    }

    validate(payload: FeedPublishOperation[1]): boolean {
        return Validator.validateString(payload.publisher)
    }
}
