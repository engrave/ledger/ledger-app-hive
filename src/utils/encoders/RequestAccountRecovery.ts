import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {RequestAccountRecoveryOperation} from '@hiveio/dhive';
import {Validator} from "./common/Validator";

export class RequestAccountRecovery extends OperationEncoder {
    encode(payload: RequestAccountRecoveryOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.recovery_account),
            Encoder.encodeString(payload.account_to_recover),
            Encoder.encodeAuthorityType(payload.new_owner_authority),
            Encoder.Array(Encoder.encodeEmpty, payload.extensions)
        ])
    }

    validate(payload: RequestAccountRecoveryOperation[1]): boolean {
        return Validator.validateString(payload.recovery_account) &&
            Validator.validateString(payload.account_to_recover) &&
            Validator.validateAuthorityType(payload.new_owner_authority)
    }

}

