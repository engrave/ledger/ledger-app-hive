import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {TransferOperation} from "@hiveio/dhive";

export class Transfer extends OperationEncoder {
    encode(payload: TransferOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.from),
            Encoder.encodeString(payload.to),
            Encoder.encodeAsset(payload.amount),
            Encoder.encodeString(payload.memo)
        ])
    }

    validate(payload: TransferOperation[1]): boolean {
        return Validator.validateString(payload.from) &&
            Validator.validateString(payload.to) &&
            Validator.validateString(payload.memo)
    }
}
