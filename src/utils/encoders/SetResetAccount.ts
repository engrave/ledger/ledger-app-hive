import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {SetResetAccountOperation} from '@hiveio/dhive';
import {Validator} from "./common/Validator";

export class SetResetAccount extends OperationEncoder {
    encode(payload: SetResetAccountOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.account),
            Encoder.encodeString(payload.current_reset_account),
            Encoder.encodeString(payload.reset_account)
        ])
    }

    validate(payload: SetResetAccountOperation[1]): boolean {
        return Validator.validateString(payload.account) &&
            Validator.validateString(payload.current_reset_account) &&
            Validator.validateString(payload.reset_account)
    }
}

