import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {AccountUpdateOperation} from "@hiveio/dhive";

export class AccountUpdate extends OperationEncoder {
    encode(payload: AccountUpdateOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.account),
            Encoder.Optional(Encoder.encodeAuthorityType, payload.owner),
            Encoder.Optional(Encoder.encodeAuthorityType, payload.active),
            Encoder.Optional(Encoder.encodeAuthorityType, payload.posting),
            Encoder.encodePublicKey(payload.memo_key),
            Encoder.encodeString(payload.json_metadata)
        ])
    }

    validate(payload: AccountUpdateOperation[1]): boolean {
        return Validator.validateString(payload.account) &&
            Validator.validateString(payload.json_metadata) &&
            Validator.validateAuthorityType(payload.owner) &&
            Validator.validateAuthorityType(payload.active) &&
            Validator.validateAuthorityType(payload.posting);
    }

}

