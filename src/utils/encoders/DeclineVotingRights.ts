import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {DeclineVotingRightsOperation} from "@hiveio/dhive";

export class DeclineVotingRights extends OperationEncoder {
    encode(payload: DeclineVotingRightsOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.account),
            Encoder.encodeBoolean(payload.decline),
        ])
    }

    validate(payload: DeclineVotingRightsOperation[1]): boolean {
        return Validator.validateString(payload.account)
    }
}
