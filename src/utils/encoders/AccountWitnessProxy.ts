import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {Validator} from "./common/Validator";
import {AccountWitnessProxyOperation} from "@hiveio/dhive";

export class AccountWitnessProxy extends OperationEncoder {
    encode(payload: AccountWitnessProxyOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.account),
            Encoder.encodeString(payload.proxy),
        ])
    }

    validate(payload: AccountWitnessProxyOperation[1]): boolean {
        return Validator.validateString(payload.account) && Validator.validateString(payload.proxy)
    }
}
