import {OperationEncoder} from '../OperationEncoder';
import {Encoder} from './common/Encoder';
import {UpdateProposalVotesOperation} from "@hiveio/dhive";
import {Validator} from "./common/Validator";

export class UpdateProposalVotes extends OperationEncoder {
    encode(payload: UpdateProposalVotesOperation[1]): Buffer {
        return Buffer.concat([
            Encoder.encodeOperationType(this.operationId),
            Encoder.encodeString(payload.voter),
            Encoder.Array(Encoder.encodeUInt64, payload.proposal_ids),
            Encoder.encodeBoolean(payload.approve),
            Encoder.Array(Encoder.encodeEmpty, payload.extensions)
        ])
    }


    validate(payload: UpdateProposalVotesOperation[1]): boolean {
        return Validator.validateString(payload.voter) &&
            Validator.validateArrayOfUInt64(payload.proposal_ids)
    }

}

