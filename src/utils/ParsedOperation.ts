import {Vote} from './encoders/Vote';
import {Transfer} from './encoders/Transfer';
import {OperationEncoder} from './OperationEncoder';
import {Comment} from './encoders/Comment';
import {AccountWitnessVote} from './encoders/AccountWitnessVote';
import {AccountWitnessProxy} from './encoders/AccountWitnessProxy';
import {ChangeRecoveryAccount} from './encoders/ChangeRecoveryAccount';
import {TransferFromSavings} from './encoders/TransferFromSavings';
import {CancelTransferFromSavings} from './encoders/CancelTransferFromSavings';
import {TransferToVesting} from './encoders/TransferToVesting';
import {WithdrawVesting} from './encoders/WithdrawVesting';
import {SetWithdrawVestingRoute} from './encoders/SetWithdrawVestingRoute';
import {ClaimAccount} from './encoders/ClaimAccount';
import {CreateClaimedAccount} from './encoders/CreateClaimedAccount';
import {LimitOrderCreate} from './encoders/LimitOrderCreate';
import {LimitOrderCancel} from './encoders/LimitOrderCancel';
import {FeedPublish} from './encoders/FeedPublish';
import {Convert} from './encoders/Convert';
import {AccountCreate} from './encoders/AccountCreate';
import {AccountUpdate} from './encoders/AccountUpdate';
import {WitnessUpdate} from './encoders/WitnessUpdate';
import {DeleteComment} from './encoders/DeleteComment';
import {RemoveProposal} from './encoders/RemoveProposal';

import {Operation, OperationName} from '@hiveio/dhive';
import {VirtualOperationName} from '@hiveio/dhive/lib/chain/operation';
import {DeclineVotingRights} from './encoders/DeclineVotingRights';
import {ResetAccount} from './encoders/ResetAccount';
import {SetResetAccount} from './encoders/SetResetAccount';
import {RequestAccountRecovery} from './encoders/RequestAccountRecovery';
import {CustomJson} from './encoders/CustomJson';
import {RecoverAccount} from './encoders/RecoverAccount';
import {ClaimRewardBalance} from './encoders/ClaimRewardBalance';
import {DelegateVestingShares} from './encoders/DelegateVestingShares';
import {CreateProposal} from './encoders/CreateProposal';
import {UpdateProposalVotes} from './encoders/UpdateProposalVotes';
import {CommentOptions} from './encoders/CommentOptions';
import {RecurrentTransfer} from './encoders/RecurrentTransfer';
import {CollateralizedConvert} from './encoders/CollateralizedConvert';
import {UpdateProposal} from './encoders/UpdateProposal';

export class ParsedOperation {

    static readonly encodersMap = new Map<OperationName | VirtualOperationName, OperationEncoder>([
        ['vote', new Vote(0)],
        ['comment', new Comment(1)],
        ['transfer', new Transfer(2)],
        ['transfer_to_vesting', new TransferToVesting(3)],
        ['withdraw_vesting', new WithdrawVesting(4)],
        ['limit_order_create', new LimitOrderCreate(5)],
        ['limit_order_cancel', new LimitOrderCancel(6)],
        ['feed_publish', new FeedPublish(7)],
        ['convert', new Convert(8)],
        ['account_create', new AccountCreate(9)],
        ['account_update', new AccountUpdate(10)],
        ['witness_update', new WitnessUpdate(11)],
        ['account_witness_vote', new AccountWitnessVote(12)],
        ['account_witness_proxy', new AccountWitnessProxy(13)],
        ['delete_comment', new DeleteComment(17)],
        ['custom_json', new CustomJson(18)],
        ['comment_options', new CommentOptions(19)],
        ['set_withdraw_vesting_route', new SetWithdrawVestingRoute(20)],
        ['claim_account', new ClaimAccount(22)],
        ['create_claimed_account', new CreateClaimedAccount(23)],
        ['request_account_recovery', new RequestAccountRecovery(24)],
        ['recover_account', new RecoverAccount(25)],
        ['change_recovery_account', new ChangeRecoveryAccount(26)],
        ['transfer_to_savings', new Transfer(32)],
        ['transfer_from_savings', new TransferFromSavings(33)],
        ['cancel_transfer_from_savings', new CancelTransferFromSavings(34)],
        ['decline_voting_rights', new DeclineVotingRights(36)],
        ['reset_account', new ResetAccount(37)],
        ['set_reset_account', new SetResetAccount(38)],
        ['claim_reward_balance', new ClaimRewardBalance(39)],
        ['delegate_vesting_shares', new DelegateVestingShares(40)],
        ['create_proposal', new CreateProposal(44)],
        ['update_proposal_votes', new UpdateProposalVotes(45)],
        ['remove_proposal', new RemoveProposal(46)],
        ['update_proposal', new UpdateProposal(47)],
        ['collateralized_convert', new CollateralizedConvert(48)],
        ['recurrent_transfer', new RecurrentTransfer(49)],
    ]);

    public static encode = (op: Operation): Buffer  => {
        const data = ParsedOperation.encodersMap.get(op[0])?.encode(op[1]);
        if (!data) {
            throw new Error('Unsupported operation type');
        }
        return data;
    }

    public static validate = (op: Operation): boolean => {
        return ParsedOperation.encodersMap.get(op[0])?.validate(op[1]) ?? false;
    }

}
