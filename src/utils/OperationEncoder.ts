export abstract class OperationEncoder {
    constructor(protected operationId: number) {
    };

    abstract encode(payload: any): Buffer;
    abstract validate(payload: any): boolean;
}
