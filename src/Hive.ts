import Transport from '@ledgerhq/hw-transport'
import {Path} from './utils/Path';
import {ParsedTransaction} from './utils/ParsedTransaction';
import {SignedTransaction, Transaction} from '@hiveio/dhive';
import {Encoder} from "./utils/encoders/common/Encoder";
import {ParsedOperation} from "./utils/ParsedOperation";

const {TransportStatusError, StatusCodes} = require('@ledgerhq/errors');

export interface Settings {
    /**
     * Determines if hash signing is enabled.
     *  */
    hashSignPolicy: boolean
}

export default class Hive {
    readonly CLA = 0xD4;

    readonly INS_GET_PUBLIC_KEY = 0x02;
    readonly INS_SIGN_TRANSACTION = 0x04;
    readonly INS_GET_APP_VERSION = 0x06;
    readonly INS_GET_APP_NAME = 0x08;
    readonly INS_SIGN_HASH = 0x10;
    readonly INS_GET_SETTINGS = 0x12;
    readonly INS_SIGN_MESSAGE = 0x14;

    readonly P1_CONFIRM = 0x01;
    readonly P1_NON_CONFIRM = 0x00;

    readonly P2_NO_CHAINCODE = 0x00;

    readonly P1_FIRST_CHUNK = 0x00;
    readonly P1_SUBSEQUENT_CHUNK = 0x80;

    readonly P2_LAST_CHUNK = 0x00;
    readonly P2_WILL_BE_MORE = 0x80;

    readonly chunkMaxSize = 200;

    transport: Transport

    constructor(transport: Transport, scrambleKey: string = 'HIVE') {
        this.transport = transport
        transport.decorateAppAPIMethods(
            this,
            ["getPublicKey", "signTransaction", "getAppVersion", "getAppName", "signHash", "getSettings"],
            scrambleKey
        )
    }

    async getPublicKey(path: string, confirm: boolean = false, prefix: string = 'STM'): Promise<string> {

        const p1 = confirm ? this.P1_CONFIRM : this.P1_NON_CONFIRM;
        const bipPath = new Path(path)
        const response = await this.transport.send(this.CLA, this.INS_GET_PUBLIC_KEY, p1, this.P2_NO_CHAINCODE, bipPath.encode())
        const offset = 1 + response[0];
        const publicKey = response
            .slice(offset + 1, offset + 1 + response[offset])
            .toString("ascii");

        return `${prefix}${publicKey.substring(3)}`;
    }

    async getAppVersion(): Promise<string> {
        const response = await this.transport.send(this.CLA, this.INS_GET_APP_VERSION, 0x00, 0x00);
        return `${response[0]}.${response[1]}.${response[2]}`;
    }

    async getSettings(): Promise<Settings> {
        const response = await this.transport.send(this.CLA, this.INS_GET_SETTINGS, 0x00, 0x00);
        return {
            hashSignPolicy: response[0] === 1,
        };
    }

    async getAppName(): Promise<string> {
        const response = await this.transport.send(this.CLA, this.INS_GET_APP_NAME, 0x00, 0x00);
        return response.slice(0, -2).toString();
    }

    async signTransaction(tx: Transaction, path: string, chainId?: string): Promise<SignedTransaction> {
        const data: Buffer = new ParsedTransaction(tx, chainId).encode();
        const signature: string = await this.exchangeData(data, path, this.INS_SIGN_TRANSACTION);
        return {
            ...tx,
            signatures: [signature]
        }
    }

    async signMessage(msg: string, path: string): Promise<string> {
        if(msg.length > 512) {
            throw new Error('Message length should be less than 512 characters');
        }
        const data = Encoder.encodeString(msg);
        return this.exchangeData(data, path, this.INS_SIGN_MESSAGE);
    }

    async signHash(digest: string, path: string): Promise<string> {
        const bipPath = new Path(path);
        const hashBuf = Buffer.from(digest, 'hex');
        const totalSize = bipPath.size * 4 + 1 + hashBuf.length;
        const apdu = Buffer.concat([
            Buffer.from([this.CLA, this.INS_SIGN_HASH, 0, 0]),
            Buffer.from([totalSize]),
            bipPath.encode(),
            Buffer.from(digest, 'hex'),
        ])
        const response = await this.transport.exchange(apdu);
        const statusWord = response.readUInt16BE(response.length - 2);
        if (statusWord !== StatusCodes.OK) {
            throw new TransportStatusError(statusWord);
        }
        return response.toString('hex').slice(0, -4);
    }

    private exchangeData = async (data: Buffer, path: string, ins: number): Promise<string> => {
        const responses = [];
        let offset = 0;
        let chunk = null;

        while (offset !== data.length) {
            if (data.length - offset > this.chunkMaxSize) {
                chunk = data.slice(offset, offset + this.chunkMaxSize);
            } else {
                chunk = data.slice(offset)
            }
            const apdu = this.generateSignApdu(
                this.CLA,
                ins,
                (offset === 0 ? this.P1_FIRST_CHUNK : this.P1_SUBSEQUENT_CHUNK),
                (offset + chunk.length === data.length ? this.P2_LAST_CHUNK : this.P2_WILL_BE_MORE),
                path,
                offset,
                chunk);

            offset += chunk.length;

            const response = await this.transport.exchange(apdu)
            const statusWord = response.readUInt16BE(response.length - 2);
            if (statusWord !== StatusCodes.OK) {
                throw new TransportStatusError(statusWord);
            }
            responses.push(response);
        }
        const signature: string = responses[responses.length - 1].toString('hex').slice(0, -4);
        return signature;
    }

    private generateSignApdu = (cla: number, ins: number, p1: number, p2: number, path: string, offset: number, chunk: any): Buffer => {
        const bipPath = new Path(path);
        const totalSize = offset === 0 ? bipPath.size * 4 + 1 + chunk.length : chunk.length

        if (offset === 0) {
            return Buffer.concat([
                Buffer.from([cla, ins, p1, p2]),
                Buffer.from([totalSize]),
                bipPath.encode(),
                chunk
            ])
        } else {
            return Buffer.concat([
                Buffer.from([cla, ins, p1, p2]),
                Buffer.from([totalSize]),
                chunk
            ])
        }
    }

    static getTransactionDigest(tx: Transaction, chainId: string = 'beeab0de00000000000000000000000000000000000000000000000000000000'): string {
        return new ParsedTransaction(tx, chainId).digest();
    }

    static isDisplayableOnDevice(tx: Transaction): boolean {
        const faultyOperationNames = [
            'claim_account',
            'create_claimed_account',
            'witness_set_properties',
            'account_update2',
            'remove_proposal',
            'update_proposal',
            'recurrent_transfer'
        ];

        if (tx.extensions && tx.extensions.length > 0) {
            return false;
        }
        if (tx.operations && tx.operations.length > 1) {
            return false;
        }

        if (faultyOperationNames.includes(tx.operations[0][0])) {
            return false;
        }

        return ParsedOperation.validate(tx.operations[0]);
    }
}
