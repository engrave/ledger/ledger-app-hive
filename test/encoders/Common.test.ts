import {Encoder} from "../../src/utils/encoders/common/Encoder";
import {expect} from "chai";

describe('Common', function () {
    describe('encodeUInt64', function () {
        it('should properly encode a number lower than uint32', () => {
            expect(Encoder.encodeUInt64(1)).to.be.deep.equal(Buffer.from([0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]));
        });

        it('should properly encode a number bigger than uint32 into little endian', function () {
            const encoded = Encoder.encodeUInt64(14294967295); // 0x3540BE3FFF
            expect(encoded).to.be.deep.equal(Buffer.from([0xff, 0xe3, 0x0b, 0x54, 0x03, 0x00, 0x00, 0x00]));
        });
    });

    describe('encodeVarint32', () => {
        it('should properly encode a number lower than 0x7f', () => {
            expect(Encoder.encodeVarint32(1)).to.be.deep.equal(Buffer.from([0x01]));
        });
        it('should properly encode a number bigger than 0x7f', () => {
            expect(Encoder.encodeVarint32(128)).to.be.deep.equal(Buffer.from([0x80, 0x01]));
        });
        it('should properly encode a number bigger than 0x3fff', () => {
            expect(Encoder.encodeVarint32(16384)).to.be.deep.equal(Buffer.from([0x80, 0x80, 0x01]));
        });
        it('should properly encode a number bigger than 0x1fffff', () => {
            expect(Encoder.encodeVarint32(2097152)).to.be.deep.equal(Buffer.from([0x80, 0x80, 0x80, 0x01]));
        });
        it('should properly encode a number bigger than 0xfffffff', () => {
            expect(Encoder.encodeVarint32(268435456)).to.be.deep.equal(Buffer.from([0x80, 0x80, 0x80, 0x80, 0x01]));
        });
        it('should properly encode a number bigger than 0x7ffffffff', () => {
            expect(Encoder.encodeVarint32(34359738368)).to.be.deep.equal(Buffer.from([0x80, 0x80, 0x80, 0x80, 0x80, 0x01]));
        });
        it('should properly encode a number bigger than 0x3ffffffffff', () => {
            expect(Encoder.encodeVarint32(4398046511104)).to.be.deep.equal(Buffer.from([0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x01]));
        });
        it('should properly encode a number bigger than 0x1ffffffffffff', () => {
            expect(Encoder.encodeVarint32(562949953421312)).to.be.deep.equal(Buffer.from([0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x01]));
        });
    });

    describe('encodeAsset', () => {
        it('should properly encode HIVE asset', () => {
            expect(Encoder.encodeAsset('0.001 HIVE')).to.be.deep.equal(Buffer.from([0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x53, 0x54, 0x45, 0x45, 0x4d, 0x00, 0x00]));
        });

        it('should properly encode HBD asset', () => {
            expect(Encoder.encodeAsset('0.001 HBD')).to.be.deep.equal(Buffer.from([0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x53, 0x42, 0x44, 0x00, 0x00, 0x00, 0x00]));
        });

        it('should properly encode VESTS asset', () => {
            expect(Encoder.encodeAsset('794.286587 VESTS')).to.be.deep.equal(Buffer.from([0xfb, 0xd9, 0x57, 0x2f, 0x00, 0x00, 0x00, 0x00, 0x06, 0x56, 0x45, 0x53, 0x54, 0x53, 0x00, 0x00]));
        });
    })

    describe('encodeString', () => {
        it('should properly encode a string', () => {
            const str = 'axxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxz';
            expect(Encoder.encodeString(str)).to.be.deep.equal(Buffer.from([129, 1, 97, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 122]));
        });
    });
});
