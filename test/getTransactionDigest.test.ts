import * as txTransfer from './transactions/transfer.json';
import * as txTransferToSavings from './transactions/transfer_to_savings.json';
import * as txTransferFromSavings from './transactions/transfer_from_savings.json';
import * as txCancelTransferFromSavings from './transactions/cancel_transfer_from_savings.json';
import * as txVote from './transactions/vote.json';
import * as txComment from './transactions/comment.json';
import * as txAccountWitnessVote from './transactions/account_witness_vote.json';
import * as txAccountWitnessProxy from './transactions/account_witness_proxy.json';
import * as txChangeRecoveryAccount from './transactions/change_recovery_account.json';
import * as txTransferToVesting from './transactions/transfer_to_vesting.json';
import * as txWithdrawVesting from './transactions/withdraw_vesting.json';
import * as txSetWithdrawVestingRoute from './transactions/set_withdraw_vesting_route.json';
import * as txClaimAccount from './transactions/claim_account.json';
import * as txCreateClaimedAccount from './transactions/create_claimed_account.json';
import * as txLimitOrderCreate from './transactions/limit_order_create.json';
import * as txLimitOrderCancel from './transactions/limit_order_cancel.json';
import * as txFeedPublish from './transactions/feed_publish.json';
import * as txConvert from './transactions/convert.json';
import * as txAccountCreate from './transactions/account_create.json';
import * as txAccountUpdate from './transactions/account_update.json';
import * as txWitnessUpdate from './transactions/witness_update.json';
import * as txDeleteComment from './transactions/delete_comment.json';
import * as txRemoveProposal from './transactions/remove_proposal.json';
import * as txDeclineVotingRights from './transactions/decline_voting_rights.json';
import * as txResetAccount from './transactions/reset_account.json';
import * as txSetResetAccount from './transactions/set_reset_account.json';
import * as txCustomJson from './transactions/custom_json.json';
import * as txRecoverAccount from './transactions/recover_account.json';
import * as txClaimRewardBalance from './transactions/claim_reward_balance.json';
import * as txDelegateVestingShares from './transactions/delegate_vesting_shares.json';
import * as txCreateProposal from './transactions/create_proposal.json';
import * as txUpdateProposalVotes from './transactions/update_proposal_votes.json';
import * as txCommentOptions from './transactions/comment_options.json';
import * as txRecurrentTransfer from './transactions/recurrent_transfer.json';
import * as txCollateralizedConvert from './transactions/collateralized_convert.json';
import * as txUpdateProposal from './transactions/update_proposal.json';

import {expect} from 'chai';
import Hive from "../src/Hive";
import {Transaction} from "@hiveio/dhive";

describe('getTransactionDigest', () => {

    const testData = [
        {tx: txTransfer, digest: '9cb27ba4dc8e7c0d16adea345eeb2e8132ea3e9ac5dce45d18f932ea9165b3fd'},
        {tx: txTransferToSavings, digest: '4a6f1c61e57de20f0fd113f184ea8ffa4d215566a987ff788526a9efa42e7cc8'},
        {tx: txTransferFromSavings, digest: '659f038cc6082a69614e9bca2644714d193a59d98836576379e24e6b7de83e45'},
        {tx: txCancelTransferFromSavings, digest: '9959d8c7796b9bc1fb06111e7d4fab0f4fe2658dc0a71039acf06c921711b0d7'},
        {tx: txVote, digest: '696e10bb815b042a2fb1a0da2a46ba5985f30b4abcf5ae6f1658dfe00747e765'},
        {tx: txComment, digest: '11f68659d4a4b613a8b0165c883c3bca2459ad8fc9c3d5c181b53ba5cd1efac4'},
        {tx: txAccountWitnessVote, digest: '03e108db83197b81d7ae993414e4b339ec159644364633a52941ac227ec0a4d0'},
        {tx: txAccountWitnessProxy, digest: '0cc0cd1d03d882181873f44e2b6d451d7a94356fd0cd74f5d5dfa0bbcbcff11d'},
        {tx: txChangeRecoveryAccount, digest: 'c69ebd2e5d3f961582ac33e02d2e524d683bfefb90679abf1ea4f7e634d43816'},
        {tx: txTransferToVesting, digest: '3a7890688818891951fd0990a0ed3211a45d18b5dd80b6c50c839e2a25a4ce57'},
        {tx: txWithdrawVesting, digest: 'cf91b73f33970eeb9e961cfb891c1d6613da6e1fea4b81c2ab6619b2fc1959e1'},
        {tx: txSetWithdrawVestingRoute, digest: '5987757e1089c23704594dcbbfd614c134ca1fc5beb93a832d79646c2ce43b28'},
        {tx: txClaimAccount, digest: 'da443de4b27822ae011c3f08f54629833d3960de795de1dc2e4d7b16eea88de6'},
        {tx: txCreateClaimedAccount, digest: 'a23406f4b54284a0ebaf380b0362172c876f52564cb432202c49d846701c70f8'},
        {tx: txLimitOrderCreate, digest: '66218a64f225b30eb1c7db0be92ec7660f84768bebe313cf835b8e795a270ae2'},
        {tx: txLimitOrderCancel, digest: 'a04c57ffcad6fd8260794afbed62ebee530d7354e723f1004ea9addb6e03e67c'},
        {tx: txFeedPublish, digest: 'f73088212ee188b9a49f541fd4c8dcbe9a4a47da76f6167d28aa3c78cf5909e2'},
        {tx: txConvert, digest: '0ff51d561d7a8cbc26fd71b53bc549d655ac5431d857e138367a389619723b4b'},
        {tx: txAccountCreate, digest: '5f58c7a63adee1a0504b1a1ce638d308cbf1cec9fbe11f5335380c7913cdeed3'},
        {tx: txAccountUpdate, digest: '18e9d37b1abee87c3450ec5b1702bebeb8b6649ac1654c67f5138afa74a62b44'},
        {tx: txWitnessUpdate, digest: '0d171c714953f30dcded198458fbcb6c6fb1281257466d5f17bb5ca721734c2c'},
        {tx: txDeleteComment, digest: '95a02889ee5b86b795b11309a8a2f7f41f1c0355ff9e8b9158a14e3a236e8c96'},
        {tx: txRemoveProposal, digest: 'bbe4505b10b8ce268cd2b62355f2713a9a3b8214010eb9be7109761035472b98'},
        {tx: txDeclineVotingRights, digest: '041c54bbd565dbd80d60c2d1c22cec3f15649ef85032023f8d911a59f7ef0609'},
        {tx: txResetAccount, digest: '954a600ec2c3d02aa41ede600086ba2b1a1de6b819aa156687d0362566f335b2'},
        {tx: txSetResetAccount, digest: 'b9410bc0ff5dde203ca922a5ae26ec1644105206391e1ef1a5148dd2d81f7d0b'},
        {tx: txCustomJson, digest: '8ceca11b9d7c38cf9c0b42983b1c3d85a2fa54c07576ae18e875ff5363d8286d'},
        {tx: txRecoverAccount, digest: 'ec4413c070c80a4ea4fb9e14f44b15a1c1535e71cee315b4799e68f49a6984eb'},
        {tx: txClaimRewardBalance, digest: '7db95c379f5312e3ab062d173325f881a9f5a6d60f8dc97adb2d9c9715475203'},
        {tx: txDelegateVestingShares, digest: 'c6f77cbd2a75b6954e56324185747a900974b95ee371822b33e7271170413c39'},
        {tx: txCreateProposal, digest: 'c5c87c801d39b99f46bfbd5def4e6f458ca3c05ce94b5be3539a541a91e96819'},
        {tx: txUpdateProposalVotes, digest: '839719a3a156e40e05b50a6081bc2eded8c8791a5bb058012ab80e6fc681b526'},
        {tx: txCommentOptions, digest: '38bb42af45cd7fee26ea34c6cc9fd2e47da9b3bc7a7d18d337ccdcd2537960f3'},
        {tx: txRecurrentTransfer, digest: '2dbaec1d86640ae06f2969558adeff215a11ce97784a0fe7ab797fa747efa116'},
        {tx: txCollateralizedConvert, digest: 'a52e74377cb904cfe65d039f2886278be49470351e6ed25ba9f13cedd895805d'},
        {tx: txUpdateProposal, digest: '244a478279a56f39ffeec3d9fa9c18a5d8baadcfc3a26305c57c1c64da7d1db9'}
    ]

    testData.forEach(({tx, digest}) => {
        it('should get transaction digest', () => {
            const result = Hive.getTransactionDigest(tx as unknown as Transaction);
            expect(result).to.be.deep.equal(digest);
        });
    });

});
