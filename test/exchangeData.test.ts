import * as chai from 'chai';
import * as sinon from 'sinon';
import sinonChai from 'sinon-chai';

chai.use(sinonChai);

const expect = chai.expect;
import Hive from "../src/Hive";
import {MockTransport} from "@ledgerhq/hw-transport-mocker";

describe('exchangeData', () => {
    it('should properly exchange buffer smaller than chunk size', async () => {
        const transport = new MockTransport(Buffer.from([0x01, 0x90, 0x00]));
        const hive = new Hive(transport);
        const spy =sinon.spy(hive, <any>'generateSignApdu');
        const result = await hive['exchangeData'](Buffer.from(Buffer.from('0'.repeat(100))), "44'/0'/0'/0/0", hive.INS_SIGN_TRANSACTION);
        expect(result).to.be.equal('01');
        expect(spy).to.have.callCount(1);
        expect(spy).to.have.calledWith(hive.CLA, hive.INS_SIGN_TRANSACTION, hive.P1_FIRST_CHUNK, hive.P2_LAST_CHUNK);
    });

    it('should properly exchange buffer bigger than chunk size', async () => {
        const transport = new MockTransport(Buffer.from([0x01, 0x90, 0x00]));
        const hive = new Hive(transport);
        const spy =sinon.spy(hive, <any>'generateSignApdu');
        const result = await hive['exchangeData'](Buffer.from(Buffer.from('0'.repeat(300))), "44'/0'/0'/0/0", hive.INS_SIGN_TRANSACTION);
        expect(result).to.be.equal('01');
        expect(spy).to.have.callCount(2);
        expect(spy).to.have.calledWith(hive.CLA, hive.INS_SIGN_TRANSACTION, hive.P1_FIRST_CHUNK, hive.P2_WILL_BE_MORE);
        expect(spy).to.have.calledWith(hive.CLA, hive.INS_SIGN_TRANSACTION, hive.P1_SUBSEQUENT_CHUNK, hive.P2_LAST_CHUNK);
    });

    it('should properly exchange buffer equal to chunk size', async () => {
        const transport = new MockTransport(Buffer.from([0x01, 0x90, 0x00]));
        const hive = new Hive(transport);
        const spy =sinon.spy(hive, <any>'generateSignApdu');
        const result = await hive['exchangeData'](Buffer.from(Buffer.from('0'.repeat(200))), "44'/0'/0'/0/0", hive.INS_SIGN_TRANSACTION);
        expect(result).to.be.equal('01');
        expect(spy).to.have.callCount(1);
        expect(spy).to.have.calledWith(hive.CLA, hive.INS_SIGN_TRANSACTION, hive.P1_FIRST_CHUNK, hive.P2_LAST_CHUNK);
    });

});
