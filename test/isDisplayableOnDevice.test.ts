import * as txTransfer from './transactions/transfer.json';
import * as txTransferToSavings from './transactions/transfer_to_savings.json';
import * as txTransferFromSavings from './transactions/transfer_from_savings.json';
import * as txCancelTransferFromSavings from './transactions/cancel_transfer_from_savings.json';
import * as txVote from './transactions/vote.json';
import * as txComment from './transactions/comment.json';
import * as txAccountWitnessVote from './transactions/account_witness_vote.json';
import * as txAccountWitnessProxy from './transactions/account_witness_proxy.json';
import * as txChangeRecoveryAccount from './transactions/change_recovery_account.json';
import * as txTransferToVesting from './transactions/transfer_to_vesting.json';
import * as txWithdrawVesting from './transactions/withdraw_vesting.json';
import * as txSetWithdrawVestingRoute from './transactions/set_withdraw_vesting_route.json';
import * as txClaimAccount from './transactions/claim_account.json';
import * as txCreateClaimedAccount from './transactions/create_claimed_account.json';
import * as txLimitOrderCreate from './transactions/limit_order_create.json';
import * as txLimitOrderCancel from './transactions/limit_order_cancel.json';
import * as txFeedPublish from './transactions/feed_publish.json';
import * as txConvert from './transactions/convert.json';
import * as txAccountCreate from './transactions/account_create.json';
import * as txAccountUpdate from './transactions/account_update.json';
import * as txWitnessUpdate from './transactions/witness_update.json';
import * as txDeleteComment from './transactions/delete_comment.json';
import * as txRemoveProposal from './transactions/remove_proposal.json';
import * as txDeclineVotingRights from './transactions/decline_voting_rights.json';
import * as txResetAccount from './transactions/reset_account.json';
import * as txSetResetAccount from './transactions/set_reset_account.json';
import * as txCustomJson from './transactions/custom_json.json';
import * as txRecoverAccount from './transactions/recover_account.json';
import * as txClaimRewardBalance from './transactions/claim_reward_balance.json';
import * as txDelegateVestingShares from './transactions/delegate_vesting_shares.json';
import * as txCreateProposal from './transactions/create_proposal.json';
import * as txUpdateProposalVotes from './transactions/update_proposal_votes.json';
import * as txCommentOptions from './transactions/comment_options.json';
import * as txRecurrentTransfer from './transactions/recurrent_transfer.json';
import * as txCollateralizedConvert from './transactions/collateralized_convert.json';
import * as txUpdateProposal from './transactions/update_proposal.json';

import {expect} from 'chai';
import Hive from "../src/Hive";

describe('isDisplayableOnDevice', () => {
    [
        txTransfer,
        txTransferToSavings,
        txTransferFromSavings,
        txCancelTransferFromSavings,
        txVote,
        txComment,
        txAccountWitnessVote,
        txAccountWitnessProxy,
        txChangeRecoveryAccount,
        txTransferToVesting,
        txWithdrawVesting,
        txSetWithdrawVestingRoute,
        txLimitOrderCreate,
        txLimitOrderCancel,
        txFeedPublish,
        txConvert,
        txAccountCreate,
        txAccountUpdate,
        txWitnessUpdate,
        txDeleteComment,
        txDeclineVotingRights,
        txResetAccount,
        txSetResetAccount,
        txCustomJson,
        txRecoverAccount,
        txClaimRewardBalance,
        txDelegateVestingShares,
        txCreateProposal,
        txUpdateProposalVotes,
        txCommentOptions,
        txCollateralizedConvert,
    ].forEach((tx) => {
        it(`should return true for ${tx.operations[0][0]}`, () => {
            expect( Hive.isDisplayableOnDevice(tx as any)).to.be.true;
        });
    });

    [
        txClaimAccount,
        txRemoveProposal,
        txCreateClaimedAccount,
        txUpdateProposal,
        txRecurrentTransfer
    ].forEach((tx) => {
       it(`should return false for ${tx.operations[0][0]}`, () => {
           expect( Hive.isDisplayableOnDevice(tx as any)).to.be.false;
       });
    });

    it('should return false for unknown operation', () => {
        expect( Hive.isDisplayableOnDevice({
            operations: [['unknown_operation', {}]]
        } as any)).to.be.false;
    });

    it('should return false for long memo', () => {
        expect( Hive.isDisplayableOnDevice({
            // @ts-ignore
            operations: [['transfer', {...txTransfer.operations[0][1], memo: 'a'.repeat(2049)}]]
        } as any)).to.be.false;
    });

    it('should return false for long json in custom_json', () => {
        expect( Hive.isDisplayableOnDevice({
            // @ts-ignore
            operations: [['custom_json', {...txCustomJson.operations[0][1], json: 'a'.repeat(2049)}]]
        } as any)).to.be.false;
    })

    it ('should return false for long permlink in comment_options', () => {
        expect( Hive.isDisplayableOnDevice({
            // @ts-ignore
            operations: [['comment_options', {...txCommentOptions.operations[0][1], permlink: 'a'.repeat(2049)}]]
        } as any)).to.be.false;
    });

    it('should return false for transaction with more than 1 operation', () => {
        expect( Hive.isDisplayableOnDevice({
            operations: [
                ['transfer', txTransfer.operations[0][1]],
                ['transfer', txTransfer.operations[0][1]]
            ]
        } as any)).to.be.false;
    });

    it('should return false for transaction with non empty extensions', () => {
        expect( Hive.isDisplayableOnDevice({
            operations: [
                ['transfer', txTransfer.operations[0][1]]
            ],
            extensions: [1]
        } as any)).to.be.false;
    });

    it('should return false for comment_options with too long beneficiaries', () => {
        const beneficiaries = new Array(11).fill({account: 'a', weight: 1});
        expect( Hive.isDisplayableOnDevice({
            // @ts-ignore
            operations: [['comment_options', {...txCommentOptions.operations[0][1], extensions: [[0, {beneficiaries}]]}]]
        } as any)).to.be.false;
    });
});
