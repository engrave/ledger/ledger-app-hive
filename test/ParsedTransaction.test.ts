import * as txTransfer from './transactions/transfer.json';
import * as txTransferToSavings from './transactions/transfer_to_savings.json';
import * as txTransferFromSavings from './transactions/transfer_from_savings.json';
import * as txCancelTransferFromSavings from './transactions/cancel_transfer_from_savings.json';
import * as txVote from './transactions/vote.json';
import * as txComment from './transactions/comment.json';
import * as txAccountWitnessVote from './transactions/account_witness_vote.json';
import * as txAccountWitnessProxy from './transactions/account_witness_proxy.json';
import * as txChangeRecoveryAccount from './transactions/change_recovery_account.json';
import * as txTransferToVesting from './transactions/transfer_to_vesting.json';
import * as txWithdrawVesting from './transactions/withdraw_vesting.json';
import * as txSetWithdrawVestingRoute from './transactions/set_withdraw_vesting_route.json';
import * as txClaimAccount from './transactions/claim_account.json';
import * as txCreateClaimedAccount from './transactions/create_claimed_account.json';
import * as txLimitOrderCreate from './transactions/limit_order_create.json';
import * as txLimitOrderCancel from './transactions/limit_order_cancel.json';
import * as txFeedPublish from './transactions/feed_publish.json';
import * as txConvert from './transactions/convert.json';
import * as txAccountCreate from './transactions/account_create.json';
import * as txAccountUpdate from './transactions/account_update.json';
import * as txWitnessUpdate from './transactions/witness_update.json';
import * as txDeleteComment from './transactions/delete_comment.json';
import * as txRemoveProposal from './transactions/remove_proposal.json';
import * as txDeclineVotingRights from './transactions/decline_voting_rights.json';
import * as txResetAccount from './transactions/reset_account.json';
import * as txSetResetAccount from './transactions/set_reset_account.json';
import * as txCustomJson from './transactions/custom_json.json';
import * as txRecoverAccount from './transactions/recover_account.json';
import * as txClaimRewardBalance from './transactions/claim_reward_balance.json';
import * as txDelegateVestingShares from './transactions/delegate_vesting_shares.json';
import * as txCreateProposal from './transactions/create_proposal.json';
import * as txUpdateProposalVotes from './transactions/update_proposal_votes.json';
import * as txCommentOptions from './transactions/comment_options.json';
import * as txRecurrentTransfer from './transactions/recurrent_transfer.json';
import * as txCollateralizedConvert from './transactions/collateralized_convert.json';
import * as txUpdateProposal from './transactions/update_proposal.json';

import {expect} from 'chai';
import {ParsedTransaction} from '../src/utils/ParsedTransaction';

describe('ParsedTransaction', () => {
    describe('encode', () => {
        it('should encode transfer', () => {
            const encodedTransaction = new ParsedTransaction(txTransfer as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104360207656e67726176650962676f726e69636b69010000000000000003535445454d00001254657374204c6564676572204e616e6f2053040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode transfer_to_savings', () => {
            const encodedTransaction = new ParsedTransaction(txTransferToSavings as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104342007656e677261766507656e6772617665010000000000000003535445454d00001254657374204c6564676572204e616e6f2053040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode transfer_from_savings', () => {
            const encodedTransaction = new ParsedTransaction(txTransferFromSavings as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104382107656e67726176656500000007656e6772617665010000000000000003535445454d00001254657374204c6564676572204e616e6f2053040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode transfer_to_vesting', () => {
            const encodedTransaction = new ParsedTransaction(txTransferToVesting as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104210307656e677261766507656e6772617665010000000000000003535445454d0000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode cancel_transfer_from_savings', () => {
            const encodedTransaction = new ParsedTransaction(txCancelTransferFromSavings as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101040d2207656e677261766565000000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode vote', () => {
            const encodedTransaction = new ParsedTransaction(txVote as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de000000000000000000000000000000000000000000000000000000000402528804049ce2ccea04047660b85e04010104200007656e677261766507656e67726176650c696e74726f64756374696f6e1027040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode comment', () => {
            const encodedTransaction = new ParsedTransaction(txComment as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de000000000000000000000000000000000000000000000000000000000402b2650404c9fcf3550404c0eb3860040101047b0107656e67726176650c696e74726f64756374696f6e0962676f726e69636b691572652d696e74726f64756374696f6e2d31323332311652653a20706f737465642077697468204c65646765722c5468697320636f6d6d656e7473206265656e207369676e65642077697468204c65646765722044657669636500040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode account_witness_vote', () => {
            const encodedTransaction = new ParsedTransaction(txAccountWitnessVote as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de0000000000000000000000000000000000000000000000000000000004029f66040426616e0b040486ee386004010104140c0962676f726e69636b6907656e677261766501040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode account_witness_proxy', () => {
            const encodedTransaction = new ParsedTransaction(txAccountWitnessProxy as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de0000000000000000000000000000000000000000000000000000000004029f66040426616e0b040486ee386004010104130d0962676f726e69636b6907656e6772617665040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode change_recovery_account', () => {
            const encodedTransaction = new ParsedTransaction(txChangeRecoveryAccount as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de0000000000000000000000000000000000000000000000000000000004029f66040426616e0b040486ee386004010104141a0962676f726e69636b6907656e677261766500040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode withdraw_vesting', () => {
            const encodedTransaction = new ParsedTransaction(txWithdrawVesting as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104190407656e6772617665f6833671000000000656455354530000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode set_withdraw_vesting_route', () => {
            const encodedTransaction = new ParsedTransaction(txSetWithdrawVestingRoute as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104161407656e67726176650962676f726e69636b690f2700040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode claim_account', () => {
            const encodedTransaction = new ParsedTransaction(txClaimAccount as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101041a1607656e6772617665b80b00000000000003535445454d000000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode create_claimed_account', () => {
            const encodedTransaction = new ParsedTransaction(txCreateClaimedAccount as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f35600401010481ca1707656e67726176650e656e67726176652e6c6564676572010000000001027e40357cba6d9f354392694ab4af20218f5a108fc8dcec28c1e166708c824067010001000000000102b2278d366ce5d411705789d2f96ffefc0c3c2836e7d0fb3c50670e3df7116edd0100010000000001021ea886a652381543560d79aeeb48be064247ccee4643be4e0e5c4d75400c4a9f01000390aadb2ce81cae48c26fd86f7ede5af18ad065aafdc470ea4c7e047e59c785d1147b226e616d65223a20224a6f686e20446f65227d00040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode limit_order_create', () => {
            const encodedTransaction = new ParsedTransaction(txLimitOrderCreate as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104320507656e67726176650a000000630400000000000003535445454d0000fa00000000000000035342440000000000f65ad07b040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode limit_order_cancel', () => {
            const encodedTransaction = new ParsedTransaction(txLimitOrderCancel as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101040d0607656e67726176650a000000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode feed_publish', () => {
            const encodedTransaction = new ParsedTransaction(txFeedPublish as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104290707656e6772617665f5000000000000000353424400000000e80300000000000003535445454d0000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode convert', () => {
            const encodedTransaction = new ParsedTransaction(txConvert as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101041d0807656e6772617665dcad7957863100000000000003535445454d0000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode account_create', () => {
            const encodedTransaction = new ParsedTransaction(txAccountCreate as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f35600401010481d909b80b00000000000003535445454d000007656e67726176650e656e67726176652e6c6564676572010000000001027e40357cba6d9f354392694ab4af20218f5a108fc8dcec28c1e166708c824067010001000000000102b2278d366ce5d411705789d2f96ffefc0c3c2836e7d0fb3c50670e3df7116edd0100010000000001021ea886a652381543560d79aeeb48be064247ccee4643be4e0e5c4d75400c4a9f01000390aadb2ce81cae48c26fd86f7ede5af18ad065aafdc470ea4c7e047e59c785d1147b226e616d65223a20224a6f686e20446f65227d040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode account_update', () => {
            const encodedTransaction = new ParsedTransaction(txAccountUpdate as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f35600401010481a10a07656e677261766501010000000001027e40357cba6d9f354392694ab4af20218f5a108fc8dcec28c1e166708c82406701000101000000000102b2278d366ce5d411705789d2f96ffefc0c3c2836e7d0fb3c50670e3df7116edd0100000390aadb2ce81cae48c26fd86f7ede5af18ad065aafdc470ea4c7e047e59c785d1217b226e616d65223a20224a534f4e204d65746164617461206368616e676564227d040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode witness_update', () => {
            const encodedTransaction = new ParsedTransaction(txWitnessUpdate as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104780b07656e67726176652768747470733a2f2f686976652e626c6f672f40656e67726176652f696e74726f64756374696f6e03c6d4d3387b5af7e534c9088615bc58efdcdffb68a1ca8010f2696fe6e52cd932b80b00000000000003535445454d000000000200e803000000000000000003535445454d0000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode delete_comment', () => {
            const encodedTransaction = new ParsedTransaction(txDeleteComment as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101041b1107656e677261766511612d706f73742d62792d656e6772617665040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode remove_proposal', () => {
            const encodedTransaction = new ParsedTransaction(txRemoveProposal as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101041b2e07656e6772617665020100000000000000020000000000000000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode decline_voting_rights', () => {
            const encodedTransaction = new ParsedTransaction(txDeclineVotingRights as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101040a2407656e677261766501040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode reset_account', () => {
            const encodedTransaction = new ParsedTransaction(txResetAccount as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101043c2507656e67726176650962676f726e69636b69010000000001027e40357cba6d9f354392694ab4af20218f5a108fc8dcec28c1e166708c8240670100040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode set_reset_account', () => {
            const encodedTransaction = new ParsedTransaction(txSetResetAccount as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101041a2607656e67726176650962676f726e69636b690668697665696f040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode custom_json', () => {
            const encodedTransaction = new ParsedTransaction(txCustomJson as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de000000000000000000000000000000000000000000000000000000000402abad04042c67d8f104040dc83c60040101046912000107656e6772617665066e6f74696679567b22636f6e74726163745061796c6f6164223a7b2273796d626f6c223a2257414956222c22746f223a22656e67726176652e636f6c64222c227175616e74697479223a22302e303031222c226d656d6f223a22227d7d040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode recover_account', () => {
            const encodedTransaction = new ParsedTransaction(txRecoverAccount as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de000000000000000000000000000000000000000000000000000000000402abad04042c67d8f104040dc83c60040101045c1907656e6772617665010000000001031e6a2404d1d4c9f3d24b820d06c1bd57fb93e473c59e4e8c5e278fa9ac0280a10100010000000001027e40357cba6d9f354392694ab4af20218f5a108fc8dcec28c1e166708c824067010000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode claim_reward_balance', () => {
            const encodedTransaction = new ParsedTransaction(txClaimRewardBalance as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104392707656e6772617665000000000000000003535445454d0000000000000000000003534244000000001d476ef0000000000656455354530000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode delegate_vesting_shares', () => {
            const encodedTransaction = new ParsedTransaction(txDelegateVestingShares as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104232807656e67726176650962676f726e69636b691d476ef0000000000656455354530000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode create_proposal', () => {
            const encodedTransaction = new ParsedTransaction(txCreateProposal as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104542c07656e67726176650962676f726e69636b69ffc35d607ff73e6278e001000000000003534244000000000f4c65646765722050726f706f73616c17656e67726176652d6c65646765722d70726f706f73616c00040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode update_proposal_votes', () => {
            const encodedTransaction = new ParsedTransaction(txUpdateProposalVotes as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101041c2d07656e6772617665027b00000000000000a6020000000000000100040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode comment_options', () => {
            const encodedTransaction = new ParsedTransaction(txCommentOptions as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101044a1307656e67726176650c696e74726f64756374696f6e00e1f505000000000353424400000000881301010100020962676f726e69636b69e8030e6e69636e69657a677275626c656dd007040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode recurrent_transfer', () => {
            const encodedTransaction = new ParsedTransaction(txRecurrentTransfer as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f356004010104393107656e67726176650962676f726e69636b69390500000000000003535445454d00001053656e742077697468206c6564676572a800040000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode collateralized_convert', () => {
            const encodedTransaction = new ParsedTransaction(txCollateralizedConvert as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de00000000000000000000000000000000000000000000000000000000040219420404ef2cd86d0404207f3560040101041d3007656e677261766501000000390500000000000003535445454d0000040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        it('should encode update_proposal', () => {
            const encodedTransaction = new ParsedTransaction(txUpdateProposal as any).encode();
            const expectedEncoding = Buffer.from('0420beeab0de000000000000000000000000000000000000000000000000000000000402ab8d04044d73107404048906e26004010104422f390500000000000007656e6772617665941402000000000003534244000000000d546573742070726f706f73616c0c696e74726f64756374696f6e01017f23f561040100', 'hex');
            expect(encodedTransaction).to.be.deep.equal(expectedEncoding);
        });

        //console.log(encodedTransaction.toString('hex'));

    })
});
