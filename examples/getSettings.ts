import TransportNodeHid from '@ledgerhq/hw-transport-node-speculos';
import Hive from '../src/Hive';

( async () => {

    console.log('Unlock your ledger and open Hive app....');
    const transport = await TransportNodeHid.open({apduPort: 40000});
    console.log(`Established transport with Ledger Nano`);

    try {
        const hive = new Hive(transport);
        const settings = await hive.getSettings();
        console.log("App settings:", JSON.stringify(settings, null, 2));
    } catch (e) {
        console.error(e);
    }
    finally {
        await transport.close();
    }

})();
