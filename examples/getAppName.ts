import TransportNodeHid from '@ledgerhq/hw-transport-node-hid';
import Hive from '../src/Hive';

( async () => {

    console.log('Unlock your ledger and open Hive app....');
    const transport = await TransportNodeHid.create();
    console.log(`Established transport with Ledger Nano`);
    try {
        const hive = new Hive(transport);
        const name = await hive.getAppName();
        console.log("App name:", name);
    } catch (e) {
        console.error(e);
    }
    finally {
        await transport.close();
    }

})();
