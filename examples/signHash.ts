import TransportNodeHid from '@ledgerhq/hw-transport-node-hid';
import Hive from '../src/Hive';
import {Client, Transaction} from "@hiveio/dhive";
import * as assert from "assert";

const testnetChainId = '4200000000000000000000000000000000000000000000000000000000000000';

const client = new Client('https://api.fake.openhive.network', {
    chainId: testnetChainId
});

const generateTransferTx = async (): Promise<Transaction> => {
    const properties = await client.database.getDynamicGlobalProperties();
    return {
        ref_block_num: properties.head_block_number,
        ref_block_prefix: Buffer.from(properties.head_block_id, 'hex').readUInt32LE(4),
        expiration: new Date(Date.now() + 6 * 60 * 1000).toISOString().slice(0, -5),
        operations: [
            [
                'transfer',
                {
                    "from": "engrave.cold",
                    "to": "engrave",
                    "amount": "0.001 HIVE",
                    "memo": "Sent with Ledger Nano S!"
                }
            ],
            [
                'transfer',
                {
                    "from": "engrave.cold",
                    "to": "engrave",
                    "amount": "0.001 HBD",
                    "memo": "Sent with Ledger Nano S!"
                }
            ]
        ],
        extensions: []
    }
}

(async () => {

    console.log('Unlock your ledger and open Hive app....');
    const transport = await TransportNodeHid.create();
    console.log(`Established transport with Ledger Nano S`);

    try {
        const hive = new Hive(transport);
        const tx = await generateTransferTx();

        const {hashSignPolicy} = await hive.getSettings();

        if (!hashSignPolicy) {
            throw new Error('Hash sign policy is not enabled on the device. Please enable it in the Hive app settings.');
        }

        if (!Hive.isDisplayableOnDevice(tx)) {
            const digest = Hive.getTransactionDigest(tx, testnetChainId);
            console.log(`Digest: ${digest}`);
            const signature = await hive.signHash(digest, `48'/13'/0'/0'/0'`);
            console.log(`Signature: ${signature}`);
            const signedTx = {...tx, signatures: [signature]};
            console.log(`Signed transaction: ${JSON.stringify(signedTx, null, 2)}`);
            const result = await client.broadcast.send(signedTx);
            console.log(result);
        } else {
            assert.fail('Should not reach this point as transaction with two operation is not displayable on the device');
        }

    } catch (e) {
        console.error(e);
    } finally {
        await transport.close();
    }

})();
