import TransportNodeHid from '@ledgerhq/hw-transport-node-speculos';
import Hive from '../src/Hive';
import {Client, Transaction} from '@hiveio/dhive';

const testnetChainId = '18dcf0a285365fc58b71f18b3d3fec954aa0c141c44e4e5cb4cf777b9eab274e';

const client = new Client('https://testnet.openhive.network', {
    addressPrefix: 'TST',
    chainId: testnetChainId
});

const generateTransferTx = async (): Promise<Transaction> => {
    const properties = await client.database.getDynamicGlobalProperties();
    return {
        ref_block_num: properties.head_block_number,
        ref_block_prefix: Buffer.from(properties.head_block_id, 'hex').readUInt32LE(4),
        expiration: new Date(Date.now() + 60 * 1000).toISOString().slice(0, -5),
        operations: [
            [
            "transfer",
            {
                "from": "acronyms",
                "to": "engrave",
                "amount": "0.001 TESTS",
                "memo": "Sent with Ledger Nano S!"
            }
        ]],
        extensions: []
    }
}

( async () => {

    console.log('Unlock your ledger and open Hive app....');
    const transport = await TransportNodeHid.open({apduPort: 40000});
    console.log(`Established transport with Ledger Nano S`);

    try {
        const hive = new Hive(transport);
        const tx = await generateTransferTx();
        const signedTransaction = await hive.signTransaction(tx, `48'/13'/0'/0'/0'`, testnetChainId);
        // const signedTransaction = await hive.signTransaction(tx, `48'/13'/0'/0'/0'`, testnetChainId);

        console.log('Signed transaction:', JSON.stringify(signedTransaction, null, 3));

        // UNCOMMENT IF YOU WANT TO BROADCAST SIGNED TRANSACTION TO HIVE TESTNET
        const result = await client.broadcast.send(signedTransaction);
        console.log(result);

    } catch (e) {
        console.error(e);
    }
    finally {
        await transport.close();
    }

})();
