import TransportNodeSpeculos from '@ledgerhq/hw-transport-node-speculos';
import Hive from '../src/Hive';
import {cryptoUtils, Signature} from "@hiveio/dhive";
import {PublicKey} from "@hiveio/dhive/lib/crypto";

const message = 'This is a message';

(async () => {
    console.log('Unlock your ledger and open Hive app....');
    const transport = await TransportNodeSpeculos.open({apduPort: 40000});
    console.log(`Established transport with Ledger Nano S`);

    try {
        const hive = new Hive(transport);
        const publicKey = await hive.getPublicKey(`48'/13'/0'/0'/0'`);
        console.log(`Public key: ${publicKey}`);

        const signature = await hive.signMessage(message, `48'/13'/0'/0'/0'`);
        console.log(`Signature: ${signature}`);

        // Verify signature
        const key = PublicKey.fromString(publicKey);
        const isSignedByKey = key.verify(cryptoUtils.sha256(message), Signature.fromString(signature));
        console.log('Is signed by private key:', isSignedByKey);

    } catch (e) {
        console.error(e);
    } finally {
        await transport.close();
    }
})();
