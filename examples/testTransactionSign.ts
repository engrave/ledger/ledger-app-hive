import TransportNodeHid from '@ledgerhq/hw-transport-node-hid';
import Hive from '../src/Hive';

// CHANGE THIS TO ANYTHING FROM test/transactions
import * as tx from '../test/transactions/comment_options.json'
( async () => {

    console.log('Unlock your ledger and open Hive app....');
    const transport = await TransportNodeHid.create();
    console.log(`Established transport with Ledger Nano`);

    try {
        const hive = new Hive(transport);
        const signedTransaction = await hive.signTransaction(tx as any, `48'/13'/0'/0'/0'`);
        console.log(JSON.stringify(signedTransaction, null, 3));
    } catch (e) {
        console.error(e);
    }
    finally {
        await transport.close();
    }

})();
