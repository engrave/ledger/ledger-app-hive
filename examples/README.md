# Hive Ledger App examples

This directory contains files that can be used to test working-copy of this repository. It will include Hive object from the source file and run an example using this object, so you don't need to publish your changes to the NPM registry to test it with your Ledger.

## How to run

```shell
ts-node {filename}
```

Example:
```shell
ts-node getAppConfiguration.ts
```
