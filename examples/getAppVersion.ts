import TransportNodeHid from '@ledgerhq/hw-transport-node-hid';
import Hive from '../src/Hive';

( async () => {

    console.log('Unlock your ledger and open Hive app....');
    const transport = await TransportNodeHid.create();
    console.log(`Established transport with Ledger Nano`);

    try {
        const hive = new Hive(transport);
        const version = await hive.getAppVersion();
        console.log("App version:", version);
    } catch (e) {
        console.error(e);
    }
    finally {
        await transport.close();
    }

})();
